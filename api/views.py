from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from api.serializers import UserSerializer, GroupSerializer
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.exceptions import MethodNotAllowed
import json
from http import HTTPStatus

@csrf_exempt
def calculate(request):
    """
    API endpoint that does calculations on a given polyline
    """
    if request.method == 'POST':
        # parse the json output from via - we extract the points of polygon shapes
        response_json = json.loads(request.body)
        for field in response_json:
            field_regions = response_json[field]['regions']
            for region in field_regions:
                shape_attributes = region['shape_attributes']
                if shape_attributes['name'] == 'polygon':
                    points = list(zip(shape_attributes["all_points_x"], shape_attributes["all_points_y"]))
                    print(f'got the polygon points {points}')
    elif request.method == 'OPTIONS':
        response = HttpResponse()
        # TODO: this is unsafe and should be restricted in a production environment
        response['Access-Control-Allow-Origin'] = request.headers.get('ORIGIN', '*')
        response['Access-Control-Allow-Methods'] = 'POST'
        response['Access-Control-Allow-Headers'] = 'content-type'
        response['Access-Control-Max-Age'] = '1728000'
        return response
    else:
        response = HttpResponse()
        response.status_code = HTTPStatus.METHOD_NOT_ALLOWED
        return response


    response = HttpResponse()
    # TODO: this is unsafe and should be restricted in a production environment
    response['Access-Control-Allow-Origin'] = request.headers.get('ORIGIN', '*')
    return response

# example of two REST objects that could be useful down the line (user and group objects)
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]
