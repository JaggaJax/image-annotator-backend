# Image annotator backend

A very WIP draft for a python django web app that acts as a backend for via (see https://www.robots.ox.ac.uk/~vgg/software/via/).

## Usage
After running

`docker-compose up`

the via web application should be available on `localhost:8000`. The django backend API is running on `localhost:8001`. Exporting the annotations in json format should perform a request to `localhost:8001/calculate` where the points from polygon shapes are parsed.
